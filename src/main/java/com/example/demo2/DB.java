package com.example.demo2;
import java.sql.*;
import java.util.ArrayList;
import java.util.Map;

public class DB {

    private Connection dbConn = null;

    private Connection getDBConn() throws ClassNotFoundException, SQLException {
        Map<String, String> env = System.getenv();
        String host = env.getOrDefault("DB_HOST", "192.168.13.100");
        String port = env.getOrDefault("DB_PORT", "3306");
        String database = env.getOrDefault("DB_NAME", "user39");
        String user = env.getOrDefault("DB_USER", "user39");
        String password = env.getOrDefault("DB_HOST", "23323");

        String connectionURL = String.format("jdbc:mysql://%s:%s/%s?serverTimezone=UTC",

                host,
                port,
                database);

        Class.forName("com.mysql.cj.jdbc.Driver");

        if (dbConn == null) {
            dbConn = DriverManager.getConnection(
                    connectionURL,
                    user,
                    password
            );
        }

        return dbConn;
    }
}